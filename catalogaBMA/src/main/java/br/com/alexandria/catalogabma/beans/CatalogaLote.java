/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alexandria.catalogabma.beans;

import br.com.alexandria.catalogabma.DAO.CatalogaDAO;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author anibal
 */
public class CatalogaLote implements Serializable {

    private int id_lote;
    private int andar;
    private String corredor;
    private int qtdFichas;
    private int qtdLivros;
    private String tipo_classificacao;
    private String arquivo_inicial;
    private String arquivo_final;
    private String login;
    private Date data_cad;
    private boolean novo = true;
    private CatalogaLote selecionado;

    public CatalogaLote getSelecionado() {
        return selecionado;
    }

    public void setSelecionado(CatalogaLote selecionado) {
        this.selecionado = selecionado;
    }

    public boolean isNovo() {
        return novo;
    }

    public void setNovo(boolean novo) {
        this.novo = novo;
    }

    public String getTipo_classificacao() {
        return tipo_classificacao;
    }

    public void setTipo_classificacao(String tipo_classificacao) {
        this.tipo_classificacao = tipo_classificacao;
    }

    public String getArquivo_inicial() {
        return arquivo_inicial;
    }

    public void setArquivo_inicial(String arquivo_inicial) {
        this.arquivo_inicial = arquivo_inicial;
    }

    public String getArquivo_final() {
        return arquivo_final;
    }

    public void setArquivo_final(String arquivo_final) {
        this.arquivo_final = arquivo_final;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getId_lote() {
        return id_lote;
    }

    public void setId_lote(int id_lote) {
        this.id_lote = id_lote;
    }

    public int getAndar() {
        return andar;
    }

    public void setAndar(int andar) {
        this.andar = andar;
    }

    public int getQtdFichas() {
        return qtdFichas;
    }

    public void setQtdFichas(int qtdFichas) {
        this.qtdFichas = qtdFichas;
    }

    public int getQtdLivros() {
        return qtdLivros;
    }

    public void setQtdLivros(int qtdLivros) {
        this.qtdLivros = qtdLivros;
    }

    public Date getData_cad() {
        return data_cad;
    }

    public void setData_cad(Date data_cad) {
        this.data_cad = data_cad;
    }

    public String getCorredor() {
        return corredor;
    }

    public void setCorredor(String corredor) {
        this.corredor = corredor;
    }

    public void pegaLotePorID(CatalogaLote bean) throws SQLException {

        CatalogaLoteBean cat = new CatalogaLoteBean();
        cat.editaLoteSelecionado(bean);

    }

    public Date formataData(Date data) throws ParseException {
        SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss");

        Date date = sf.parse(data.toString());
        return date;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alexandria.catalogabma.beans;

import br.com.alexandria.catalogabma.DAO.FichaDAO;
import br.com.alexandria.catalogabma.DAO.ItemFichaDAO;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author anibal
 */
public class FichaBean {

    private int codficha;
    private int codLote;
    private String titulo;
    private String autor;
    private String editora;
    private String ano;
    private String ed;
    private String ISBN;
    private String serie;
    private String complemento;
    private String notasB;
    private String notasC;
    private String assuntos;
    private String NAE;
    private String NAT;
    private String classificacao;
    private String subtitulo;
    private String responsabilidade;
    private String notacao;
    private String detalhe;
    private String LBN;
    private String status;
    private String textoCompleto;
    private String url;
    private List< ItemFichaBean>  listaItens;

    public FichaBean()  {
    }
    
    public List<ItemFichaBean> getListaItens() {
        return listaItens;
    }

    public void setListaItens(List<ItemFichaBean> listaItens) {
        this.listaItens = listaItens;
    }

    

    public int getCodficha() {
        return codficha;
    }

    public void setCodficha(int codficha) {
        this.codficha = codficha;
    }

    public int getCodLote() {
        return codLote;
    }

    public void setCodLote(int codLote) {
        this.codLote = codLote;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getEd() {
        return ed;
    }

    public void setEd(String ed) {
        this.ed = ed;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getNotasB() {
        return notasB;
    }

    public void setNotasB(String notasB) {
        this.notasB = notasB;
    }

    public String getNotasC() {
        return notasC;
    }

    public void setNotasC(String notasC) {
        this.notasC = notasC;
    }

    public String getAssuntos() {
        return assuntos;
    }

    public void setAssuntos(String assuntos) {
        this.assuntos = assuntos;
    }

    public String getNAE() {
        return NAE;
    }

    public void setNAE(String NAE) {
        this.NAE = NAE;
    }

    public String getNAT() {
        return NAT;
    }

    public void setNAT(String NAT) {
        this.NAT = NAT;
    }

    public String getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(String classificacao) {
        this.classificacao = classificacao;
    }

    public String getSubtitulo() {
        return subtitulo;
    }

    public void setSubtitulo(String subtitulo) {
        this.subtitulo = subtitulo;
    }

    public String getResponsabilidade() {
        return responsabilidade;
    }

    public void setResponsabilidade(String responsabilidade) {
        this.responsabilidade = responsabilidade;
    }

    public String getNotacao() {
        return notacao;
    }

    public void setNotacao(String notacao) {
        this.notacao = notacao;
    }

    public String getDetalhe() {
        return detalhe;
    }

    public void setDetalhe(String detalhe) {
        this.detalhe = detalhe;
    }

    public String getLBN() {
        return LBN;
    }

    public void setLBN(String LBN) {
        this.LBN = LBN;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTextoCompleto() {
        return textoCompleto;
    }

    public void setTextoCompleto(String textoCompleto) {
        this.textoCompleto = textoCompleto;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}

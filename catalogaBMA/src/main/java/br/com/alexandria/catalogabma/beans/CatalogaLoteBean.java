/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alexandria.catalogabma.beans;

import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import br.com.alexandria.catalogabma.DAO.CatalogaDAO;
import br.com.alexandria.catalogabma.services.ConfListaService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import javax.inject.Inject;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author anibal
 */
@ManagedBean(name = "catalogaBean")
@SessionScoped
public class CatalogaLoteBean {

    private List<String> tipos_cla;
    private List<String> corredores;
    private List<Integer> andares;
    private CatalogaLote catal_lote = new CatalogaLote();
    

    public CatalogaLoteBean() {
    }

    

    public CatalogaLote getCatal_lote() {
        return catal_lote;
    }

    public void setCatal_lote(CatalogaLote catal_lote) {
        this.catal_lote = catal_lote;
    }

    public List<String> getTipos_cla() {
        return tipos_cla;
    }

    public void setTipos_cla(List<String> tipos_cla) {
        this.tipos_cla = tipos_cla;
    }

    public List<String> getCorredores() {
        return corredores;
    }

    public void setCorredores(List<String> corredores) {
        this.corredores = corredores;
    }

    public List<Integer> getAndares() {
        return andares;
    }

    public void setAndares(List<Integer> andares) {
        this.andares = andares;
    }

// /*   @Inject
//    private ConfListaService service;*/
    @PostConstruct
    public void init() {

        ConfListaService service = new ConfListaService();
        service.init();
        andares = service.getAndarBeans();
        corredores = service.getCorredorBeans();
        tipos_cla = service.getTipoClaBeans();

//    ComboBox Andares
        SelectItemGroup andaresSelect = new SelectItemGroup("Andares");
        andaresSelect.setSelectItems(geraListaSelectItemsInt(andares));

//    ComboBox Corredores
        SelectItemGroup corredoresSelect = new SelectItemGroup("Corredores");
        corredoresSelect.setSelectItems(geraListaSelectItemsString(corredores));

//    ComboBox TipoCla
        SelectItemGroup tipo_cla_Select = new SelectItemGroup("Tipo classificaçao");
        tipo_cla_Select.setSelectItems(geraListaSelectItemsString(tipos_cla));
    }

//    Metodo generico que preenche as combobox de String
    public static SelectItem[] geraListaSelectItemsString(List<String> lista) {
        SelectItem[] itens = new SelectItem[lista.size()];
        for (int i = 0; i < lista.size(); i++) {
            SelectItem sel = new SelectItem(lista.get(i), lista.get(i));
            itens[i] = sel;
        }

        return itens;
    }

    public static SelectItem[] geraListaSelectItemsInt(List<Integer> lista) {
        SelectItem[] itens = new SelectItem[lista.size()];
        for (int i = 0; i < lista.size(); i++) {
            SelectItem sel = new SelectItem(lista.get(i), lista.get(i).toString());
            itens[i] = sel;
        }

        return itens;
    }

    public void insereLote() throws SQLException, IOException {

        CatalogaDAO dao = new CatalogaDAO();
        boolean sucesso = dao.insereLote(catal_lote);
        if (sucesso) {
            catal_lote.setNovo(false);
            CatalogaLoteBean cat = new CatalogaLoteBean();
            cat.mostraMsgSucesso(catal_lote.getId_lote(), "inserido");
        }
    }

    public void updateLote() throws SQLException, IOException {

        CatalogaDAO dao = new CatalogaDAO();
        boolean sucesso = dao.updateLote(catal_lote);
        if (sucesso) {
            catal_lote.setNovo(false);
            CatalogaLoteBean cat = new CatalogaLoteBean();
            cat.mostraMsgSucesso(catal_lote.getId_lote(), "atualizado");
        }
    }

    public void geraNovoLote() throws SQLException {
        catal_lote = new CatalogaLote();

    }

    public void zeraAlteracoes() throws SQLException {
        catal_lote = CatalogaDAO.selectLotePorID(catal_lote.getId_lote());
        catal_lote.setNovo(false);

    }

    public void editaLoteSelecionado(CatalogaLote bean) throws SQLException {
        catal_lote = bean;
        catal_lote.setNovo(false);

    }

    public boolean verificaCamposObrigatorios(CatalogaLote bean) throws SQLException {

        if (bean.getAndar() < 7 || bean.getAndar() > 22) {
            mostraMsgErroRequired("andar");
            return false;
        }
        if (bean.getQtdFichas() == 0) {
            mostraMsgErroRequired("Quantidade de fichas");
            return false;
        }
        if (bean.getQtdLivros() == 0) {
            mostraMsgErroRequired("Quantidade de livros");
            return false;
        }

        if (bean.getCorredor().isEmpty()) {
            mostraMsgErroRequired("corredor");
            return false;
        }

        return true;

    }

    public void mostraMsgErroIdLote(int lote) {
        String explicacao = "Id Lote " + lote + " ja existe. Tente outro ID Lote";
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "ID Duplicado", explicacao);
        RequestContext.getCurrentInstance().showMessageInDialog(message);

    }

    public static void mostraMsgErroRequired(String campo) {
        String explicacao = "O Campo " + campo + " deve ser preenchido";
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Campos obrigatorios", explicacao);
        RequestContext.getCurrentInstance().showMessageInDialog(message);

    }

    public void mostraMsgSucesso(int lote, String operacao) {
        String explicacao = "Id Lote " + lote + " " + operacao + " com sucesso!";
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso!!!", explicacao);
        RequestContext.getCurrentInstance().showMessageInDialog(message);

    }

    public void mudaNomeArqInicial() {
        String arquivo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("arquivo");
        if (arquivo != null) {
            catal_lote.setArquivo_inicial(arquivo);
        }
    }

    public void mudaNomeArqFinal() {
        String arquivo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("arquivo");
        if (arquivo != null) {
            catal_lote.setArquivo_final(arquivo);
        }
    }

}

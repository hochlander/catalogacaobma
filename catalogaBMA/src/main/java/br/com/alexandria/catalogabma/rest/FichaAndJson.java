/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alexandria.catalogabma.rest;

import br.com.alexandria.catalogabma.DAO.CatalogaDAO;
import br.com.alexandria.catalogabma.DAO.FichaDAO;
import br.com.alexandria.catalogabma.beans.CatalogaLote;
import br.com.alexandria.catalogabma.beans.FichaBean;
import br.com.alexandria.catalogabma.beans.QueryBean;
import static br.com.alexandria.catalogabma.rest.CatalogaBeanAndJson.catalogaLoteToJson;
import static br.com.alexandria.catalogabma.rest.CatalogaBeanAndJson.stringToDate;
import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anibal
 */
public class FichaAndJson {

    public static FichaBean jsonToFicha(JSONObject jsob) throws JSONException {
        FichaBean ficha = new FichaBean();
        if (jsob.has("codficha") && jsob.getInt("codficha") != 0) {
            ficha.setCodficha(jsob.getInt("codficha"));
        }
        if (jsob.has("codLote") && jsob.getInt("codLote") != 0) {
            ficha.setCodLote(jsob.getInt("codLote"));
        }
        if (jsob.has("titulo") && jsob.getString("titulo") != "") {
            ficha.setTitulo(jsob.getString("titulo"));
        }
        if (jsob.has("autor") && jsob.getString("autor") != "") {
            ficha.setAutor(jsob.getString("autor"));
        }
        if (jsob.has("editora") && jsob.getString("editora") != "") {
            ficha.setEditora(jsob.getString("editora"));
        }
        if (jsob.has("ano") && jsob.getString("ano") != "") {
            ficha.setAno(jsob.getString("ano"));
        }
        if (jsob.has("ed") && jsob.getString("ed") != "") {
            ficha.setEd(jsob.getString("ed"));
        }
        if (jsob.has("ISBN") && jsob.getString("ISBN") != "") {
            ficha.setISBN(jsob.getString("ISBN"));
        }
        if (jsob.has("serie") && jsob.getString("serie") != "") {
            ficha.setSerie(jsob.getString("serie"));
        }
        if (jsob.has("complemento") && jsob.getString("complemento") != "") {
            ficha.setComplemento(jsob.getString("complemento"));
        }
        if (jsob.has("notasC") && jsob.getString("notasC") != "") {
            ficha.setNotasC(jsob.getString("notasC"));
        }
        if (jsob.has("assuntos") && jsob.getString("assuntos") != "") {
            ficha.setAssuntos(jsob.getString("assuntos"));
        }
        if (jsob.has("NAE") && jsob.getString("NAE") != "") {
            ficha.setNAE(jsob.getString("NAE"));
        }
        if (jsob.has("NAT") && jsob.getString("NAT") != "") {
            ficha.setNAT(jsob.getString("NAT"));
        }
        if (jsob.has("classificacao") && jsob.getString("classificacao") != "") {
            ficha.setClassificacao(jsob.getString("classificacao"));
        }
        if (jsob.has("subtitulo") && jsob.getString("subtitulo") != "") {
            ficha.setSubtitulo(jsob.getString("subtitulo"));
        }
        if (jsob.has("responsabilidade") && jsob.getString("responsabilidade") != "") {
            ficha.setResponsabilidade(jsob.getString("responsabilidade"));
        }
        if (jsob.has("notacao") && jsob.getString("notacao") != "") {
            ficha.setNotacao(jsob.getString("notacao"));
        }
        if (jsob.has("detalhe") && jsob.getString("detalhe") != "") {
            ficha.setDetalhe(jsob.getString("detalhe"));
        }
        if (jsob.has("LBN") && jsob.getString("LBN") != "") {
            ficha.setLBN(jsob.getString("LBN"));
        }

        if (jsob.has("status") && jsob.getString("status") != "") {
            ficha.setStatus(jsob.getString("status"));
        }
        if (jsob.has("textoCompleto") && jsob.getString("textoCompleto") != "") {
            ficha.setTextoCompleto(jsob.getString("textoCompleto"));
        }
        if (jsob.has("url") && jsob.getString("url") != "") {
            ficha.setUrl(jsob.getString("url"));
        }

        return ficha;

    }

    public static JSONObject fichaToJson(FichaBean bean) throws JSONException {
        JSONObject jsob = new JSONObject();
        if (bean.getCodficha() >= 0) {
            jsob.put("codficha", bean.getCodficha());
        }
        if (bean.getCodLote() >= 0) {
            jsob.put("codLote", bean.getCodLote());
        }

        if (!(bean.getTitulo() == null) && !(bean.getTitulo().equalsIgnoreCase(""))) {
            jsob.put("titulo", bean.getTitulo());
        }
        if (!(bean.getAutor() == null) && !(bean.getAutor().equalsIgnoreCase(""))) {
            jsob.put("autor", bean.getAutor());
        }
        if (!(bean.getEditora() == null) && !(bean.getEditora().equalsIgnoreCase(""))) {
            jsob.put("editora", bean.getEditora());
        }
        if (!(bean.getAno() == null) && !(bean.getAno().equalsIgnoreCase(""))) {
            jsob.put("ano", bean.getAno());
        }
        if (!(bean.getEd() == null) && !(bean.getEd().equalsIgnoreCase(""))) {
            jsob.put("ed", bean.getEd());
        }
        if (!(bean.getISBN() == null) && !(bean.getISBN().equalsIgnoreCase(""))) {
            jsob.put("ISBN", bean.getISBN());
        }
        if (!(bean.getSerie() == null) && !(bean.getSerie().equalsIgnoreCase(""))) {
            jsob.put("serie", bean.getSerie());
        }
        if (!(bean.getComplemento() == null) && !(bean.getComplemento().equalsIgnoreCase(""))) {
            jsob.put("complemento", bean.getComplemento());
        }
        if (!(bean.getNotasB() == null) && !(bean.getNotasB().equalsIgnoreCase(""))) {
            jsob.put("notasB", bean.getNotasB());
        }
        if (!(bean.getNotasC() == null) && !(bean.getNotasC().equalsIgnoreCase(""))) {
            jsob.put("notasC", bean.getNotasC());
        }
        if (!(bean.getAssuntos() == null) && !(bean.getAssuntos().equalsIgnoreCase(""))) {
            jsob.put("assuntos", bean.getAssuntos());
        }
        if (!(bean.getNAE() == null) && !(bean.getNAE().equalsIgnoreCase(""))) {
            jsob.put("NAE", bean.getNAE());
        }
        if (!(bean.getNAT() == null) && !(bean.getNAT().equalsIgnoreCase(""))) {
            jsob.put("NAT", bean.getNAT());
        }
        if (!(bean.getClassificacao() == null) && !(bean.getClassificacao().equalsIgnoreCase(""))) {
            jsob.put("classificacao", bean.getClassificacao());
        }
        if (!(bean.getSubtitulo() == null) && !(bean.getSubtitulo().equalsIgnoreCase(""))) {
            jsob.put("subtitulo", bean.getSubtitulo());
        }
        if (!(bean.getResponsabilidade() == null) && !(bean.getResponsabilidade().equalsIgnoreCase(""))) {
            jsob.put("responsabilidade", bean.getResponsabilidade());
        }
        if (!(bean.getNotacao() == null) && !(bean.getNotacao().equalsIgnoreCase(""))) {
            jsob.put("notacao", bean.getNotacao());
        }
        if (!(bean.getDetalhe() == null) && !(bean.getDetalhe().equalsIgnoreCase(""))) {
            jsob.put("detalhe", bean.getDetalhe());
        }
        if (!(bean.getLBN() == null) && !(bean.getLBN().equalsIgnoreCase(""))) {
            jsob.put("LBN", bean.getLBN());
        }
        if (!(bean.getStatus() == null) && !(bean.getStatus().equalsIgnoreCase(""))) {
            jsob.put("status", bean.getStatus());
        }
        if (!(bean.getTextoCompleto() == null) && !(bean.getTextoCompleto().equalsIgnoreCase(""))) {
            jsob.put("textoCompleto", bean.getTextoCompleto());
        }
        if (!(bean.getUrl() == null) && !(bean.getUrl().equalsIgnoreCase(""))) {
            jsob.put("url", bean.getUrl());
        }
        
//        listaItensFicha??

        return jsob;

    }

    public static List<FichaBean> jsonToListaFichas(JSONObject jsob) throws JSONException, ParseException {
        List<FichaBean> listaFichas = new LinkedList<>();
        JSONArray jsArr = jsob.getJSONArray("data");
        for (int i = 0; i < jsArr.length(); i++) {
            JSONObject single = jsArr.getJSONObject(i);
            FichaBean ficha = jsonToFicha(single);
            listaFichas.add(ficha);
        }
        return listaFichas;
    }

    public static JSONObject pegaFichasPeloLote(CatalogaLote lote) throws JSONException, ParseException {
        JSONObject jsob = new JSONObject();

        List<FichaBean> listaFichas = new LinkedList<>();

        try {
            listaFichas = FichaDAO.selectFichaPorIntervalo(lote);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(QueryBean.class.getName()).log(Level.SEVERE, null, ex);

        }
        if (listaFichas.size() > 0) {
            JSONArray jsArr = new JSONArray();
            for (FichaBean ficha : listaFichas) {
                JSONObject unit = fichaToJson(ficha);
                jsArr.put(unit);
            }
            jsob.put("data", jsArr);
        }
        String jsobStr = jsob.toString();
        return jsob;
    }

    public static void main(String[] args) throws JSONException, ParseException {
//        Teste cliente listaFichasToJson e JsonToListaFichas
        CatalogaLote lote = new CatalogaLote();
        lote.setArquivo_inicial("Producao/12_Andar-20210916-124337758-00001.jpg");
        lote.setArquivo_final("Producao/12_Andar-20210916-124339303-00003.jpg");
        JSONObject json1 = pegaFichasPeloLote(lote);
        List<FichaBean> lsitaFichas = jsonToListaFichas(json1);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alexandria.catalogabma.rest;

import br.com.alexandria.catalogabma.DAO.CatalogaDAO;
import br.com.alexandria.catalogabma.beans.CatalogaLote;
import br.com.alexandria.catalogabma.beans.QueryBean;
import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static br.com.alexandria.catalogabma.rest.QueryBeanAndJson.dateStringParser;

/**
 *
 * @author anibal
 */
public class CatalogaBeanAndJson {

    public static CatalogaLote jsonToCatalogaLote(JSONObject jsob) throws JSONException, ParseException {
        CatalogaLote lote = new CatalogaLote();

        if (jsob.has("id_Lote") && jsob.getInt("id_Lote") != 0) {
            lote.setId_lote(jsob.getInt("id_Lote"));
        }

        if (jsob.has("andar") && jsob.getInt("andar") != 0) {
            lote.setAndar(jsob.getInt("andar"));
        }
        if (jsob.has("qteFichas") && jsob.getInt("qteFichas") != 0) {
            lote.setQtdFichas(jsob.getInt("qteFichas"));
        }
        if (jsob.has("qteLivros") && jsob.getInt("qteLivros") != 0) {
            lote.setQtdLivros(jsob.getInt("qteLivros"));
        }

        if (jsob.has("corredor") && jsob.getString("corredor") != "") {
            lote.setCorredor(jsob.getString("corredor"));
        }

        if (jsob.has("arquivoIni") && jsob.getString("arquivoIni") != "") {
            lote.setArquivo_inicial(jsob.getString("arquivoIni"));
        }
        if (jsob.has("arquivoFinal") && jsob.getString("arquivoFinal") != "") {
            lote.setArquivo_final(jsob.getString("arquivoFinal"));
        }
        if (jsob.has("tipoCla") && jsob.getString("tipoCla") != "") {
            lote.setTipo_classificacao(jsob.getString("tipoCla"));
        }
        if (jsob.has("login") && jsob.getString("login") != "") {
            lote.setLogin(jsob.getString("login"));
        }

        if (jsob.has("data_cad") && jsob.getString("data_cad") != "") {
            lote.setData_cad(stringToDate(jsob.getString("data_cad")));
        }

        return lote;
    }

    public static JSONObject catalogaLoteToJson(CatalogaLote lote) throws JSONException, ParseException {
        JSONObject jsob = new JSONObject();

        if (lote.getAndar() != 0) {
            jsob.put("andar", lote.getAndar());
        }
        if (lote.getId_lote() != 0) {
            jsob.put("id_Lote", lote.getId_lote());
        }
        if (lote.getQtdFichas() != 0) {
            jsob.put("qteFichas", lote.getQtdFichas());
        }
        if (lote.getQtdLivros() != 0) {
            jsob.put("qteLivros", lote.getQtdLivros());
        }
        if (!lote.getCorredor().equalsIgnoreCase("")) {
            jsob.put("corredor", lote.getCorredor());
        }
        if ( ! (lote.getArquivo_inicial()==null) && !(lote.getArquivo_inicial().equalsIgnoreCase(""))) {
            jsob.put("arquivoIni", lote.getArquivo_inicial());
        }
        if ( ! (lote.getArquivo_final()==null) && !(lote.getArquivo_final().equalsIgnoreCase(""))) {
            jsob.put("arquivoFinal", lote.getArquivo_final());
        }
        if ( ! (lote.getTipo_classificacao()==null) && !(lote.getTipo_classificacao().equalsIgnoreCase(""))) {
            jsob.put("tipoCla", lote.getTipo_classificacao());
        }
        if ( ! (lote.getLogin()==null) && !(lote.getLogin().equalsIgnoreCase("")) ) {
            jsob.put("login", lote.getLogin());
        }
        if (lote.getData_cad() != null) {
            jsob.put("data_cad", dateToString(lote.getData_cad()));
        }

        return jsob;
    }

    public static JSONObject geraJsonLotesPorIntervaloData(QueryBean bean) throws JSONException, ParseException {
        JSONObject jsob = new JSONObject();
        
        bean.setData_ini(CatalogaDAO.utilToSQL(bean.getData_ini()));
        bean.setData_fim(CatalogaDAO.utilToSQL(bean.getData_fim()));

        List<CatalogaLote> listaLotes = new LinkedList<>();

        try {
            listaLotes = CatalogaDAO.selectLotesQuery(bean);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(QueryBean.class.getName()).log(Level.SEVERE, null, ex);

        }
        if (listaLotes.size() > 0) {
            JSONArray jsArr = new JSONArray();
            for (CatalogaLote lote : listaLotes) {
                JSONObject unit = catalogaLoteToJson(lote);
                jsArr.put(unit);
            }
            jsob.put("data", jsArr);
        }
        String jsobStr = jsob.toString();
        return jsob;
    }

    public static Date stringToDate(String str) throws ParseException {
        Date data = new SimpleDateFormat("dd/MM/yyyy").parse(str);
        return data;
    }

    public static String dateToString(Date data) throws ParseException {
        String str = new SimpleDateFormat("dd/MM/yyyy").format(data);
        return str;
    }
    
    public static void main(String[] args) throws JSONException, ParseException {
//        teste pesquisa lote
        QueryBean query = new QueryBean();
        query.setAndar(8);
        query.setCorredor("1B");
        geraJsonLotesPorIntervaloData(query);
    }

}

package br.com.alexandria.catalogabma.views;

import br.com.alexandria.catalogabma.beans.ConfListaBean;
import br.com.alexandria.catalogabma.services.ConfListaService;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import javax.inject.Inject;
import javax.inject.Named;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author anibal
 */
@Named
@RequestScoped
public class SelectOneMenuView {
    
    private List<ConfListaBean> andares;
    private List<ConfListaBean> corredores;
    private List<ConfListaBean> tipos_cla;
    private String andar;
    private String corredor;
    private String tipo_cla;
    
    @Inject
    private ConfListaService service;
    
    @PostConstruct
    public void init() {
        

//    ComboBox Andares
        SelectItemGroup andaresSelect = new SelectItemGroup("Andares");
        andaresSelect.setSelectItems(geraListaSelectItems(andares));        
  
//    ComboBox Corredores
        SelectItemGroup corredoresSelect = new SelectItemGroup("Corredores");
        corredoresSelect.setSelectItems(geraListaSelectItems(corredores));
        
//    ComboBox TipoCla
        SelectItemGroup tipo_cla_Select = new SelectItemGroup("Tipo classificaçao");
        tipo_cla_Select.setSelectItems(geraListaSelectItems(tipos_cla));
    }
    
//    Metodo generico que preenche as combobox
    public static SelectItem[] geraListaSelectItems(List<ConfListaBean> lista) {
        SelectItem[] itens = new SelectItem[lista.size()];
        for (int i = 0; i < lista.size(); i++) {
            SelectItem sel = new SelectItem(lista.get(i).getValor(), lista.get(i).getValor());
            itens[i] = sel;
        }
        
        return itens;
    }
    
    public List<ConfListaBean> getAndares() {
        return andares;
    }
    
    public void setAndares(List<ConfListaBean> andares) {
        this.andares = andares;
    }
    
    public List<ConfListaBean> getCorredores() {
        return corredores;
    }
    
    public void setCorredores(List<ConfListaBean> corredores) {
        this.corredores = corredores;
    }
    
    public List<ConfListaBean> getTipos_cla() {
        return tipos_cla;
    }
    
    public void setTipos_cla(List<ConfListaBean> tipos_cla) {
        this.tipos_cla = tipos_cla;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getCorredor() {
        return corredor;
    }

    public void setCorredor(String corredor) {
        this.corredor = corredor;
    }

    public String getTipo_cla() {
        return tipo_cla;
    }

    public void setTipo_cla(String tipo_cla) {
        this.tipo_cla = tipo_cla;
    }
    

    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alexandria.catalogabma.DAO;

import static br.com.alexandria.catalogabma.DAO.CatalogaDAO.sqlToUtil;
import br.com.alexandria.catalogabma.beans.CatalogaLote;
import br.com.alexandria.catalogabma.beans.FichaBean;
import br.com.alexandria.catalogabma.rest.FichaAndJson;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author anibal
 */
public class FichaDAO {

    public static List<FichaBean> selectFichaPorIntervalo(CatalogaLote lote) throws SQLException {
        String ini = "";
        String fim = "";
        try {
            if (!(lote.getArquivo_inicial().equalsIgnoreCase("")) && !(lote.getArquivo_inicial() == null)) {
                ini = lote.getArquivo_inicial();
            }
            if (!(lote.getArquivo_final().equalsIgnoreCase("")) && !(lote.getArquivo_final() == null)) {
                fim = lote.getArquivo_final();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<FichaBean> lista = new LinkedList<FichaBean>();
        Connection conn = CatalogaDAO.getConnection();
        String sql = "select * from ficha where url >= '"
                + ini
                + "' and url <= '"
                + fim
                + "' ";
        Statement stmt = conn.createStatement();
        ResultSet rs;
        rs = stmt.executeQuery(sql);

        while (rs.next()) {

            FichaBean bean = new FichaBean();
            bean.setCodficha(rs.getInt("codficha"));
            bean.setCodLote(rs.getInt("codLote"));
            bean.setTitulo(rs.getString("titulo"));
            bean.setAutor(rs.getString("autor"));
            bean.setEditora(rs.getString("editora"));
            bean.setAno(rs.getString("ano"));
            bean.setEd("ed");
            bean.setISBN(rs.getString("ISBN"));
            bean.setSerie(rs.getString("serie"));
            bean.setComplemento(rs.getString("complemento"));
            bean.setNotasB(rs.getString("notasB"));
            bean.setNotasC(rs.getString("notasC"));
            bean.setAssuntos(rs.getString("assuntos"));
            bean.setNAE(rs.getString("NAE"));
            bean.setNAT(rs.getString("NAT"));
            bean.setClassificacao(rs.getString("classificacao"));
            bean.setSubtitulo(rs.getString("subtitulo"));
            bean.setResponsabilidade(rs.getString("responsabilidade"));
            bean.setNotacao(rs.getString("notacao"));
            bean.setDetalhe(rs.getString("detalhe"));
            bean.setLBN(rs.getString("LBN"));
            bean.setStatus(rs.getString("status"));
            bean.setTextoCompleto(rs.getString("textoCompleto"));
            bean.setUrl(rs.getString("url"));
            
             try {
                    bean.setListaItens(ItemFichaDAO.selectItemFichaPorID(bean.getCodficha()));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            lista.add(bean);

        }

        conn.close();
        return lista;
    }

    public static FichaBean selectFichaPorID(int codficha) throws SQLException {
        FichaBean bean = new FichaBean();
        Connection conn = CatalogaDAO.getConnection();
        String sql = "select * from ficha where codficha = " + codficha;
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs;
            rs = stmt.executeQuery(sql);

            while (rs.next()) {

                bean.setCodficha(rs.getInt("codficha"));
                bean.setCodLote(rs.getInt("codLote"));
                bean.setTitulo(rs.getString("titulo"));
                bean.setAutor(rs.getString("autor"));
                bean.setEditora(rs.getString("editora"));
                bean.setAno(rs.getString("ano"));
                bean.setEd("ed");
                bean.setISBN(rs.getString("ISBN"));
                bean.setSerie(rs.getString("serie"));
                bean.setComplemento(rs.getString("complemento"));
                bean.setNotasB(rs.getString("notasB"));
                bean.setNotasC(rs.getString("notasC"));
                bean.setAssuntos(rs.getString("assuntos"));
                bean.setNAE(rs.getString("NAE"));
                bean.setNAT(rs.getString("NAT"));
                bean.setClassificacao(rs.getString("classificacao"));
                bean.setSubtitulo(rs.getString("subtitulo"));
                bean.setResponsabilidade(rs.getString("responsabilidade"));
                bean.setNotacao(rs.getString("notacao"));
                bean.setDetalhe(rs.getString("detalhe"));
                bean.setLBN(rs.getString("LBN"));
                bean.setStatus(rs.getString("status"));
                bean.setTextoCompleto(rs.getString("textoCompleto"));
                bean.setUrl(rs.getString("url"));

                try {
                    bean.setListaItens(ItemFichaDAO.selectItemFichaPorID(bean.getCodficha()));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }
        return bean;
    }

    public static void updateFichaPorID(int codficha, String status) throws SQLException {

        Connection conn = CatalogaDAO.getConnection();
        String sql = "UPDATE ficha SET status = '"
                + status
                + "' WHERE codficha = "
                + codficha;
        try {
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(sql);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }
    }

    public static void main(String[] args) throws SQLException, JSONException, ParseException {
        selectFichaPorID(2);

        FichaDAO dao = new FichaDAO();
        updateFichaPorID(2, "processado");
    }
}

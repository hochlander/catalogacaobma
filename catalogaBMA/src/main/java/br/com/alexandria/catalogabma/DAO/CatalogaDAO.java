/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alexandria.catalogabma.DAO;

import br.com.alexandria.catalogabma.beans.CatalogaLote;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import br.com.alexandria.catalogabma.beans.CatalogaLoteBean;
import br.com.alexandria.catalogabma.beans.QueryBean;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anibal
 */
/**
 * create table alx_lote_ficha ( id_lote integer primary key not null, andar
 * integer, corredor Varchar(20) , qtdFichas integer, qtdLivros integer ,
 * tipo_cla Varchar(10) , arquivo_inicial Varchar(250) , arquivo_final
 * Varchar(250) , data_cad datetime , login varchar(20) );
 *
 */
public class CatalogaDAO {

    String diverClass = "";
    String jdbcUrl = "";
    String user = "";
    String password = "";
    InputStream inputStream;

    private static ComboPooledDataSource cpds = new ComboPooledDataSource();

    static {
        try {
            CatalogaDAO dao = new CatalogaDAO();
            dao.init();

//            cpds.setDriverClass("net.sourceforge.jtds.jdbc.Driver");
//            cpds.setJdbcUrl("jdbc:jtds:sqlserver://192.168.0.172:1433/smc");
//            cpds.setUser("sa");
//            cpds.setPassword("*marx2012*");
        } // handle the exception
        catch (IOException ex) {
            Logger.getLogger(CatalogaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Connection getConnection() throws SQLException {
        return cpds.getConnection();
    }

    public CatalogaDAO() {

    }

    public void init() throws IOException {

        try {
            Properties prop = new Properties();
             inputStream = new FileInputStream("/home/anibal/docsanibal/catalogaBMA.properties");

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file not found in the classpath");
            }

            // get the property value and print it out
            diverClass = prop.getProperty("diverClass");
            jdbcUrl = prop.getProperty("jdbcUrl");
            user = prop.getProperty("user");
            password = prop.getProperty("password");

            cpds.setDriverClass(diverClass);
            cpds.setJdbcUrl(jdbcUrl);
            cpds.setUser(user);
            cpds.setPassword(password);
            cpds.setMaxPoolSize(10);
            Class.forName(diverClass);

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            inputStream.close();
        }
    }

    public static boolean insereLote(CatalogaLote bean) throws SQLException {
        CatalogaLoteBean bb = new CatalogaLoteBean();
        boolean preenchido = bb.verificaCamposObrigatorios(bean);

        if (preenchido) {

            boolean coroa = true;
            Connection conn = CatalogaDAO.getConnection();
            try {

                String sql = "INSERT INTO alx_lote_ficha (id_lote, andar, corredor, "
                        + "qtdFichas, qtdLivros, tipo_cla, arquivo_inicial, "
                        + "arquivo_final, data_cad, login)"
                        + "VALUES (?,?,?,    ?,?,?,    ?,?,?,  ?)";

                PreparedStatement prep = conn.prepareStatement(sql);
                prep.setInt(1, bean.getId_lote());
                prep.setInt(2, bean.getAndar());
                prep.setString(3, bean.getCorredor());
                prep.setInt(4, bean.getQtdFichas());
                prep.setInt(5, bean.getQtdLivros());
                prep.setString(6, bean.getTipo_classificacao());
                prep.setString(7, bean.getArquivo_inicial());
                prep.setString(8, bean.getArquivo_final());
                prep.setDate(9, utilToSQL(new Date(System.currentTimeMillis())));
                prep.setString(10, bean.getLogin());
                prep.execute();
            } catch (SQLException e) {
                System.out.println(e);
                if (e.getMessage().contains("Violation of PRIMARY KEY")) {
                    bb.mostraMsgErroIdLote(bean.getId_lote());
                }
                coroa = false;
            } finally {
                conn.close();
            }
            return coroa;
        } else {
            return false;
        }
    }

    public static boolean updateLote(CatalogaLote bean) throws SQLException {

        CatalogaLoteBean bb = new CatalogaLoteBean();
        boolean preenchido = bb.verificaCamposObrigatorios(bean);
        if (preenchido) {
            boolean coroa = true;
            Connection conn = CatalogaDAO.getConnection();
            try {
                int id = bean.getId_lote();

                String sql = "UPDATE alx_lote_ficha set andar=?, corredor=?, "
                        + "qtdFichas=?, qtdLivros=?, tipo_cla=?, "
                        + "arquivo_inicial=?, arquivo_final=?, data_cad=?, "
                        + "login=?"
                        + "where id_lote = " + id;

                PreparedStatement prep = conn.prepareStatement(sql);
                prep.setInt(1, bean.getAndar());
                prep.setString(2, bean.getCorredor());
                prep.setInt(3, bean.getQtdFichas());
                prep.setInt(4, bean.getQtdLivros());
                prep.setString(5, bean.getTipo_classificacao());
                prep.setString(6, bean.getArquivo_inicial());
                prep.setString(7, bean.getArquivo_final());
                prep.setDate(8, utilToSQL(new Date(System.currentTimeMillis())));
                prep.setString(9, bean.getLogin());
                prep.execute();
            } catch (SQLException e) {
                coroa = false;
                System.out.println(e);
                if (e.getMessage().contains("Violation of PRIMARY KEY")) {
                    bb.mostraMsgErroIdLote(bean.getId_lote());
                }
            } finally {
                conn.close();
            }
            return coroa;
        } else {
            return false;
        }
    }

    public static CatalogaLote selectLotePorID(int id) throws SQLException {
        CatalogaLote bean = new CatalogaLote();
        Connection conn = CatalogaDAO.getConnection();
        String sql = "select * from alx_lote_ficha where id_lote = " + id;
        Statement stmt = conn.createStatement();
        ResultSet rs;
        rs = stmt.executeQuery(sql);

        while (rs.next()) {
            bean.setId_lote(rs.getInt("id_lote"));
            bean.setAndar(rs.getInt("andar"));
            bean.setCorredor(rs.getString("corredor"));
            bean.setQtdFichas(rs.getInt("qtdFichas"));
            bean.setQtdLivros(rs.getInt("qtdLivros"));
            bean.setTipo_classificacao(rs.getString("tipo_cla"));
            bean.setArquivo_inicial(rs.getString("arquivo_inicial"));
            bean.setArquivo_final(rs.getString("arquivo_final"));
            bean.setData_cad(sqlToUtil(rs.getDate("data_cad")));
            bean.setLogin(rs.getString("login"));
        }

        conn.close();
        return bean;
    }

    public static List<CatalogaLote> selectTodosLotes() throws SQLException {
        List<CatalogaLote> lista = new LinkedList<CatalogaLote>();
        Connection conn = CatalogaDAO.getConnection();
        String sql = "select * from alx_lote_ficha";
        Statement stmt = conn.createStatement();
        ResultSet rs;
        rs = stmt.executeQuery(sql);

        while (rs.next()) {
            CatalogaLote bean = new CatalogaLote();
            bean.setId_lote(rs.getInt("id_lote"));
            bean.setAndar(rs.getInt("andar"));
            bean.setCorredor(rs.getString("corredor"));
            bean.setQtdFichas(rs.getInt("qtdFichas"));
            bean.setQtdLivros(rs.getInt("qtdLivros"));
            bean.setTipo_classificacao(rs.getString("tipo_cla"));
            bean.setArquivo_inicial(rs.getString("arquivo_inicial"));
            bean.setArquivo_final(rs.getString("arquivo_final"));
            bean.setData_cad(sqlToUtil(rs.getDate("data_cad")));
            bean.setLogin(rs.getString("login"));
            lista.add(bean);
        }

        conn.close();
        return lista;
    }

    public static List<CatalogaLote> selectLotesQuery(QueryBean query) throws SQLException {
        List<CatalogaLote> lista = new LinkedList<CatalogaLote>();
        Connection conn = CatalogaDAO.getConnection();
        try {

            int elementos = 0;
            String sql = " select * from alx_lote_ficha ";
            if (query.getId_lote() > 0) {
                elementos++;
                sql += " where id_lote = " + query.getId_lote();
            }
            if (query.getAndar() > 6) {
                elementos++;
                if (elementos > 1) {
                    sql += " and andar = " + query.getAndar();
                } else {
                    sql += " where andar = " + query.getAndar();
                }
            }

            if (query.getCorredor() != null && query.getCorredor() != "") {
                elementos++;
                if (elementos > 1) {
                    sql += " and corredor = '" + query.getCorredor() + "'";
                } else {
                    sql += " where corredor = '" + query.getCorredor() + "'";
                }
            }

            if (query.getData_ini() != null) {
                elementos++;
                if (elementos > 1) {
                    if (query.getData_ini() != null) {
                        sql += " and data_cad >= '" + query.getData_ini() + "'";
                    }
                } else {
                    sql += " where data_cad >= '" + query.getData_ini() + "'";
                }
            }

            if (query.getData_fim() != null) {
                elementos++;
                if (elementos > 1) {
                    if (query.getData_fim() != null) {
                        sql += " and data_cad <= '" + query.getData_fim() + "'";
                    }
                } else {
                    sql += " where data_cad <= '" + query.getData_fim() + "'";
                }
            }

            Statement stmt = conn.createStatement();
            ResultSet rs;
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CatalogaLote bean = new CatalogaLote();
                bean.setId_lote(rs.getInt("id_lote"));
                bean.setAndar(rs.getInt("andar"));
                bean.setCorredor(rs.getString("corredor"));
                bean.setQtdFichas(rs.getInt("qtdFichas"));
                bean.setQtdLivros(rs.getInt("qtdLivros"));
                bean.setTipo_classificacao(rs.getString("tipo_cla"));
                bean.setArquivo_inicial(rs.getString("arquivo_inicial"));
                bean.setArquivo_final(rs.getString("arquivo_final"));
                bean.setData_cad(sqlToUtil(rs.getDate("data_cad")));
                bean.setLogin(rs.getString("login"));
                lista.add(bean);
            }
        } catch (SQLException e) {
            System.out.println(e);
        } finally {

            conn.close();
        }

        return lista;
    }

    public static java.sql.Date utilToSQL(Date date) {
        if (date != null) {

            java.sql.Date sqlDate = new java.sql.Date(date.getTime());
            return sqlDate;
        }
        return null;
    }

    public static java.util.Date sqlToUtil(java.sql.Date date) {
        if (date != null) {
            Date utilDate = new java.util.Date(date.getTime());
            return utilDate;
        }
        return null;
    }

    public static void main(String[] args) throws SQLException, IOException {
        CatalogaDAO dao = new CatalogaDAO();
        CatalogaLote lote = new CatalogaLote();
        lote.setArquivo_inicial("Producao/12_Andar-20210916-124337758-00001.jpg");
        lote.setArquivo_final("Producao/12_Andar-20210916-124339303-00003.jpg");
        lote.setId_lote(2);
        lote.setAndar(11);
        lote.setCorredor("5B");
        lote.setQtdFichas(44);
        lote.setQtdLivros(88);
        insereLote(lote);
             
    }
}

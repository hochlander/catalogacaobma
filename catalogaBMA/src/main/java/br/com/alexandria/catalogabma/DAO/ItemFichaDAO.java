/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alexandria.catalogabma.DAO;

import static br.com.alexandria.catalogabma.DAO.CatalogaDAO.sqlToUtil;
import br.com.alexandria.catalogabma.beans.CatalogaLote;
import br.com.alexandria.catalogabma.beans.ItemFichaBean;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author anibal
 */
public class ItemFichaDAO {
        public static List<ItemFichaBean> selectItemFichaPorID(int codFicha) throws SQLException {
        List <ItemFichaBean> lista = new  LinkedList<ItemFichaBean>();
        Connection conn = CatalogaDAO.getConnection();
        String sql = "select * from Itemficha where codficha = " + codFicha;
        Statement stmt = conn.createStatement();
        ResultSet rs;
        rs = stmt.executeQuery(sql);

        while (rs.next()) {         

            ItemFichaBean bean = new ItemFichaBean();
            bean.setCodficha(rs.getInt("codFicha"));
            bean.setIndice(rs.getInt("indice"));
            bean.setCodtit(rs.getInt("codtit"));
            bean.setTitulo(rs.getString("titulo"));
            bean.setAutor(rs.getString("autor"));
            bean.setEditora(rs.getString("editora"));
            bean.setEd("ed");
            
            lista.add(bean);

        }

        conn.close();
        return lista;
    }

        
        public static void main(String[] args) throws SQLException {
        ItemFichaDAO dao = new ItemFichaDAO();
        dao.selectItemFichaPorID(2);
       
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alexandria.catalogabma.rest;

import br.com.alexandria.catalogabma.DAO.CatalogaDAO;
import br.com.alexandria.catalogabma.beans.CatalogaLote;
import br.com.alexandria.catalogabma.beans.QueryBean;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anibal
 */
public class QueryBeanAndJson {

    public JSONObject queryBeanToJson(QueryBean query) throws JSONException, ParseException {
        JSONObject jsob = new JSONObject();

        if (query.getAndar() != 0) {
            jsob.put("andar", query.getAndar());
        }
        if (!query.getCorredor().equalsIgnoreCase("")) {
            jsob.put("corredor", query.getCorredor());
        }
        if (query.getData_ini() != null) {
            jsob.put("data_ini", dateToString(query.getData_ini()));
        }
        if (query.getData_fim() != null) {
            jsob.put("data_fim", dateToString(query.getData_ini()));
        }
        if (query.getId_lote() != 0) {
            jsob.put("id_Lote", query.getId_lote());
        }
        String jsobStr = jsob.toString();
        return jsob;
    }

    public QueryBean jsonToQueryBean(JSONObject jsob) throws JSONException, ParseException {
        QueryBean query = new QueryBean();

        if (jsob.has("id_Lote") && jsob.getInt("id_Lote") != 0) {
            query.setId_lote(jsob.getInt("id_Lote"));
        }

        if (jsob.has("andar") && jsob.getInt("andar") != 0) {
            query.setAndar(jsob.getInt("andar"));
        }

        if (jsob.has("corredor") && jsob.getString("corredor") != "") {
            query.setCorredor(jsob.getString("corredor"));
        }

        if (jsob.has("data_ini") && jsob.getString("data_ini") != "") {
            query.setData_ini(stringToDate(jsob.getString("data_ini")));
        }

        if (jsob.has("data_fim") && jsob.getString("data_fim").length() > 0) {
            query.setData_fim(stringToDate(jsob.getString("data_fim")));
        }

        return query;
    }

    public static QueryBean stringToQueryBean(String idloteString, String andarString, String corredor, String datainiString, String datafimString) throws ParseException {
        QueryBean query = new QueryBean();

        if (!(idloteString == null) && !(idloteString.equalsIgnoreCase(""))) {
            int idLote = Integer.parseInt(idloteString);
            query.setId_lote(idLote);
        }
        if (!(andarString == null) && !(andarString.equalsIgnoreCase(""))) {
            int andar = Integer.parseInt(andarString);
            query.setAndar(andar);
        }
        if (!(corredor == null) && !(corredor.equalsIgnoreCase(""))) {
            query.setCorredor(corredor);
        }
        if (!(datainiString == null) && !(datainiString.equalsIgnoreCase(""))) {
            Date data_ini = stringToDate(datainiString);
            query.setData_ini(data_ini);
        }
        if (!(datafimString == null) && !(datafimString.equalsIgnoreCase(""))) {
            Date data_fim= stringToDate(datafimString);
            query.setData_fim(data_fim);
        }

        return query;
    }

    public static String dateStringParser(String str) throws ParseException {
        Date data = new SimpleDateFormat("dd/MM/yyyy").parse(str);
        String data2 = new SimpleDateFormat("yyyy-MM-dd").format(data);
        return data2;
    }

    public static java.util.Date formataData(Date data) throws ParseException {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");

        java.util.Date date = sf.parse(data.toString());
        return date;
    }

    public static java.util.Date stringToDate(String str) throws ParseException {
        java.util.Date data = new SimpleDateFormat("yyyy-MM-dd").parse(str);

     
        return data;
    }

    public static String dateToString(Date data) throws ParseException {
        String str = new SimpleDateFormat("yyyy-MM-dd").format(data);
        return str;
    }

//    DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
//String strDate = dateFormat.format(date);
    public static void main(String[] args) throws JSONException, ParseException {
        QueryBean query = new QueryBean();
        query.setAndar(10);
        query.setCorredor("B");
        query.setId_lote(20);
        Date hoje = new Date();
        query.setData_ini(hoje);

        QueryBeanAndJson qq = new QueryBeanAndJson();
        JSONObject obj = qq.queryBeanToJson(query);
    }

}

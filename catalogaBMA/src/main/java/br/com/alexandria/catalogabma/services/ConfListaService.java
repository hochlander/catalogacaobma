/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alexandria.catalogabma.services;

import br.com.alexandria.catalogabma.DAO.ConfListaDAO;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.inject.Named;

/**
 *
 * @author anibal
 */
@Named
@ApplicationScoped
public class ConfListaService {

    
    private List<Integer> andarBeans;  
    private List<String> corredorBeans;
    private List<String> tipoClaBeans;
    String andar = "Retro_andar";
    String corredor = "Retro_corredor";
    String tipo_cla = "Retro_tipo_cla";

    @PostConstruct
    public void init() {
        ConfListaDAO dao = new ConfListaDAO();

        try {
            preencheConfLista();
        } catch (SQLException e) {
            System.out.println(e);
        }

    }

    public List<Integer> getAndarBeans() {
            return andarBeans;
    }

    public void setAndarBeans(List<Integer> andarBeans) {
        this.andarBeans = andarBeans;
    }



    public List<String> getCorredorBeans() {
        return corredorBeans;
    }

    public void setCorredorBeans(List<String> corredorBeans) {
        this.corredorBeans = corredorBeans;
    }

    public List<String> getTipoClaBeans() {
        return tipoClaBeans;
    }

    public void setTipoClaBeans(List<String> tipoClaBeans) {
        this.tipoClaBeans = tipoClaBeans;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getCorredor() {
        return corredor;
    }

    public void setCorredor(String corredor) {
        this.corredor = corredor;
    }

    public String getTipo_cla() {
        return tipo_cla;
    }

    public void setTipo_cla(String tipo_cla) {
        this.tipo_cla = tipo_cla;
    }

    public void preencheConfLista() throws SQLException {
        ConfListaDAO dao = new ConfListaDAO();
        andarBeans = dao.selectTodosPorCampoInt(andar);
        corredorBeans = dao.selectTodosPorCampoString(corredor);
        tipoClaBeans = dao.selectTodosPorCampoString(tipo_cla);
    }
}

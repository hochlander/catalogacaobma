/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alexandria.catalogabma.rest;

import br.com.alexandria.catalogabma.DAO.FichaDAO;
import static br.com.alexandria.catalogabma.DAO.FichaDAO.selectFichaPorID;
import static br.com.alexandria.catalogabma.DAO.ItemFichaDAO.selectItemFichaPorID;
import br.com.alexandria.catalogabma.beans.CatalogaLote;
import br.com.alexandria.catalogabma.beans.FichaBean;
import br.com.alexandria.catalogabma.beans.ItemFichaBean;
import br.com.alexandria.catalogabma.beans.QueryBean;
import static br.com.alexandria.catalogabma.rest.FichaAndJson.fichaToJson;
import static br.com.alexandria.catalogabma.rest.FichaAndJson.jsonToFicha;
import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anibal
 */
public class ItemFichaAndJson {

    public static JSONObject itemFichaToJson(ItemFichaBean bean) throws JSONException {
        JSONObject jsob = new JSONObject();
        if (bean.getCodficha() >= 0) {
            jsob.put("codficha", bean.getCodficha());
        }
        if (bean.getIndice() >= 0) {
            jsob.put("indice", bean.getIndice());
        }

        if (bean.getCodtit() >= 0) {
            jsob.put("codtit", bean.getCodtit());
        }

        if (!(bean.getTitulo() == null) && !(bean.getTitulo().equalsIgnoreCase(""))) {
            jsob.put("titulo", bean.getTitulo());
        }
        if (!(bean.getAutor() == null) && !(bean.getAutor().equalsIgnoreCase(""))) {
            jsob.put("autor", bean.getAutor());
        }
        if (!(bean.getEditora() == null) && !(bean.getEditora().equalsIgnoreCase(""))) {
            jsob.put("editora", bean.getEditora());
        }

        if (!(bean.getEd() == null) && !(bean.getEd().equalsIgnoreCase(""))) {
            jsob.put("ed", bean.getEd());
        }

        return jsob;

    }

    public static ItemFichaBean jsonToItemFicha(JSONObject jsob) throws JSONException {
        ItemFichaBean bean = new ItemFichaBean();

        if (jsob.has("codficha") && jsob.getInt("codficha") != 0) {
            bean.setCodficha(jsob.getInt("codficha"));
        }
        if (jsob.has("indice") && jsob.getInt("indice") != 0) {
            bean.setIndice(jsob.getInt("indice"));
        }
        if (jsob.has("codtit") && jsob.getInt("codtit") != 0) {
            bean.setCodtit(jsob.getInt("codtit"));
        }
        if (jsob.has("titulo") && jsob.getString("titulo") != "") {
            bean.setTitulo(jsob.getString("titulo"));
        }
        if (jsob.has("autor") && jsob.getString("autor") != "") {
            bean.setAutor(jsob.getString("autor"));
        }
        if (jsob.has("editora") && jsob.getString("editora") != "") {
            bean.setEditora(jsob.getString("editora"));
        }

        if (jsob.has("ed") && jsob.getString("ed") != "") {
            bean.setEd(jsob.getString("ed"));
        }

        return bean;

    }

    public static JSONObject geraJsonItensPeloIdFicha(int idFicha) throws JSONException, ParseException, SQLException {
        JSONObject jsob = new JSONObject();
        List<ItemFichaBean> listaItens = new LinkedList<>();

        try {
            listaItens = selectItemFichaPorID(idFicha);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(QueryBean.class.getName()).log(Level.SEVERE, null, ex);

        }
        if (listaItens.size() > 0) {
            JSONArray jsArr = new JSONArray();
            for (ItemFichaBean item : listaItens) {
                JSONObject unit = itemFichaToJson(item);
                jsArr.put(unit);
            }
            jsob.put("data", jsArr);
        }
        String jsobStr = jsob.toString();
        return jsob;
    }

    public static List<ItemFichaBean> jsonToListaItens(JSONObject jsob) throws JSONException, ParseException {
        List<ItemFichaBean> listaItens = new LinkedList<>();
        JSONArray jsArr = jsob.getJSONArray("data");
        for (int i = 0; i < jsArr.length(); i++) {
            JSONObject single = jsArr.getJSONObject(i);
            ItemFichaBean ficha = jsonToItemFicha(single);
            listaItens.add(ficha);
        }
        return listaItens;
    }

    public static void main(String[] args) throws SQLException, JSONException, ParseException {
//        teste para Item Ficha. Serviço: pegaFichasLotes
        JSONObject jsonListaItens = geraJsonItensPeloIdFicha(2);
        List<ItemFichaBean> listaItens = jsonToListaItens(jsonListaItens);

    }

}

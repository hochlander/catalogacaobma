/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alexandria.catalogabma.beans;

import br.com.alexandria.catalogabma.DAO.CatalogaDAO;
import br.com.alexandria.catalogabma.services.ConfListaService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItemGroup;

/**
 *
 * @author anibal
 */
@ManagedBean(name = "queyBean")
@SessionScoped
public class QueryBean {

    private String corredor;
    private int id_lote;
    private int andar;
    private Date data_ini;
    private Date data_fim;
    private List<String> corredores;
    private List<Integer> andares;
    private List<CatalogaLote> listaLotes;
    private CatalogaLote selecionado = new CatalogaLote();

  

    public QueryBean() {
    }

    @PostConstruct
    public void init() {

        ConfListaService service = new ConfListaService();
        service.init();
        andares = service.getAndarBeans();
        corredores = service.getCorredorBeans();

//    ComboBox Andares
        SelectItemGroup andaresSelect = new SelectItemGroup("Andares");
        andaresSelect.setSelectItems(CatalogaLoteBean.geraListaSelectItemsInt(andares));

//    ComboBox Corredores
        SelectItemGroup corredoresSelect = new SelectItemGroup("Corredores");
        corredoresSelect.setSelectItems(CatalogaLoteBean.geraListaSelectItemsString(corredores));

    }


    public List<String> getCorredores() {
        return corredores;
    }

    public void setCorredores(List<String> corredores) {
        this.corredores = corredores;
    }

    public List<Integer> getAndares() {
        return andares;
    }

    public void setAndares(List<Integer> andares) {
        this.andares = andares;
    }

    public String getCorredor() {
        return corredor;
    }

    public void setCorredor(String corredor) {
        this.corredor = corredor;
    }
    
      public CatalogaLote getSelecionado() {
        return selecionado;
    }

    public void setSelecionado(CatalogaLote selecionado) {
        this.selecionado = selecionado;
    }

    public int getId_lote() {
        return id_lote;
    }

    public void setId_lote(int id_lote) {
        this.id_lote = id_lote;
    }

    public int getAndar() {
        return andar;
    }

    public void setAndar(int andar) {
        this.andar = andar;
    }

    public Date getData_ini() {
        return data_ini;
    }

    public void setData_ini(Date data_ini) {
        this.data_ini = data_ini;
    }

    public Date getData_fim() {
        return data_fim;
    }

    public void setData_fim(Date data_fim) {
        this.data_fim = data_fim;
    }

    public List<CatalogaLote> getListaLotes() {
        return listaLotes;
    }

    public void setListaLotes(List<CatalogaLote> listaLotes) {
        this.listaLotes = listaLotes;
    }

    public void pegaLotePorQuery()  {
        System.out.println("pesquisar    ");
        QueryBean bean = new QueryBean();
        CatalogaDAO cata = new CatalogaDAO();
        bean.setId_lote(id_lote);
        bean.setAndar(andar);
        bean.setCorredor(corredor);
        bean.setData_ini(CatalogaDAO.utilToSQL(data_ini));
        bean.setData_fim(CatalogaDAO.utilToSQL(data_fim));

        try {
            listaLotes = CatalogaDAO.selectLotesQuery(bean);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(QueryBean.class.getName()).log(Level.SEVERE, null, ex);
            
            
        }

    }
   
   


}

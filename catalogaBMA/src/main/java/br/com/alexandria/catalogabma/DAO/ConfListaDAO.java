/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alexandria.catalogabma.DAO;

import br.com.alexandria.catalogabma.beans.ConfListaBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author anibal
 */
public class ConfListaDAO {

    public static void insereTodosAndares() throws SQLException {
        Connection conn = CatalogaDAO.getConnection();

        for (int i = 7; i < 23; i++) {
            String sql = "INSERT INTO alx_conf_lista (campo, valor)"
                    + "VALUES ('Retro_andar',"
                    + i
                    + ")";
            PreparedStatement prep = conn.prepareStatement(sql);
            prep.execute();

        }
        conn.close();
    }

    public static void insereTodosCorredores() throws SQLException {
        Connection conn = CatalogaDAO.getConnection();
        String letra = "";
        for (int i = 1; i < 11; i++) {
            for (int j = 0; j < 2; j++) {
                if (j == 0) {
                    letra = "A";
                } else {
                    letra = "B";
                }
                String corredor = i + letra;
                String sql = "INSERT INTO alx_conf_lista (campo, valor)"
                        + "VALUES ('Retro_corredor', '"
                        + corredor
                        + "')";
                PreparedStatement prep = conn.prepareStatement(sql);
                prep.execute();
            }

        }
        conn.close();
    }

    public static void insereTodosTiposCla() throws SQLException {
        Connection conn = CatalogaDAO.getConnection();
        String tipo_cla = "";
        for (int j = 0; j < 3; j++) {
            if (j == 0) {
                tipo_cla = "Fixa";
            }
            if (j == 1) {
                tipo_cla = "Mista";
            }
            if (j == 2) {
                tipo_cla = "Arte";
            }
            String sql = "INSERT INTO alx_conf_lista (campo, valor)"
                    + "VALUES ('Retro_tipo_cla', '"
                    + tipo_cla
                    + "')";
            PreparedStatement prep = conn.prepareStatement(sql);
            prep.execute();
        }

        conn.close();
    }

    public static void insereUmConfBean(ConfListaBean bean) throws SQLException {
        Connection conn = CatalogaDAO.getConnection();

        String sql = "INSERT INTO alx_conf_lista (campo, valor)"
                + "VALUES ('"
                + bean.getCampo()
                + "', '"
                + bean.getValor()
                + "')";
        PreparedStatement prep = conn.prepareStatement(sql);
        prep.execute();
        conn.close();
    }

    public static List<String> selectTodosPorCampoString(String chave) throws SQLException {
        List<String> lista = new LinkedList<>();
        Connection conn = CatalogaDAO.getConnection();
        String sql = "select * from alx_conf_lista where campo = '"
                + chave
                + "'";
        Statement stmt = conn.createStatement();
        ResultSet rs;
        rs = stmt.executeQuery(sql);
        while (rs.next()) {
            ConfListaBean bean = new ConfListaBean();
            String valor = rs.getString("valor");
            lista.add(valor);
        }
        conn.close();
        return lista;

    }

    public static List<Integer> selectTodosPorCampoInt(String chave) throws SQLException {
        List<Integer> lista = new LinkedList<>();
        Connection conn = CatalogaDAO.getConnection();
        String sql = "select * from alx_conf_lista where campo = '"
                + chave
                + "'";
        Statement stmt = conn.createStatement();
        ResultSet rs;
        rs = stmt.executeQuery(sql);
        while (rs.next()) {
            ConfListaBean bean = new ConfListaBean();
            Integer valor = rs.getInt("valor");
            lista.add(valor);
        }
        conn.close();
        return lista;

    }

    public static void main(String[] args) throws SQLException {
        ConfListaBean bean = new ConfListaBean();
        insereTodosAndares();
        insereTodosCorredores();
        insereTodosTiposCla();   
    }

}

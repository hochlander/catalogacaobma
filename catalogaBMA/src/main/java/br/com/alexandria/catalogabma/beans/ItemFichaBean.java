/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alexandria.catalogabma.beans;

/**
 *
 * @author anibal
 */
public class ItemFichaBean {

    private int codficha;
    private int indice;
    private int codtit;
    private String titulo;
    private String autor;
    private String editora;
    private String ed;

    public int getCodficha() {
        return codficha;
    }

    public void setCodficha(int codficha) {
        this.codficha = codficha;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    public int getCodtit() {
        return codtit;
    }

    public void setCodtit(int codtit) {
        this.codtit = codtit;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public String getEd() {
        return ed;
    }

    public void setEd(String ed) {
        this.ed = ed;
    }

}

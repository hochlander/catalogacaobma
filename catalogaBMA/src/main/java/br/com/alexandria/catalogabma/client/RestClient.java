/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alexandria.catalogabma.client;

import br.com.alexandria.catalogabma.DAO.CatalogaDAO;
import br.com.alexandria.catalogabma.beans.CatalogaLote;
import br.com.alexandria.catalogabma.beans.FichaBean;
import br.com.alexandria.catalogabma.rest.CatalogaBeanAndJson;
import br.com.alexandria.catalogabma.rest.FichaAndJson;
import static br.com.alexandria.catalogabma.rest.FichaAndJson.jsonToFicha;
import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.apache.http.client.utils.URIBuilder;

/**
 *
 * @author anibal
 */
public class RestClient {

    public static boolean respostaUpdate(String input) throws MalformedURLException, IOException, JSONException {
        boolean achou = false;
        String retorno = "";
        URL url = new URL(input);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        int status = con.getResponseCode();
        if (status == 200) {
            achou = true;
        }
        return achou;
    }

    public static JSONObject respostaJson(String input) throws MalformedURLException, IOException, JSONException {
        boolean achou = false;
        String retorno = "";
        URL url = new URL(input);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        int status = con.getResponseCode();
        if (status == 200) {
            retorno = pegaRespostaUrl(con);
            JSONObject jsob = new JSONObject(retorno);
            return jsob;
        }
        return null;

    }

    public static String pegaRespostaUrl(HttpURLConnection con) throws MalformedURLException, IOException {
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        return content.toString();
    }

    public static List<CatalogaLote> pesquisaLote(int id_Lote, int andar, String corredor, String data_ini, String data_fim)
            throws IOException, SQLException, JSONException, ParseException, URISyntaxException {

        String idLoteString = "";
        String andarString = "";
        try {
            if (id_Lote > 0) {
                idLoteString = String.valueOf(id_Lote);
            }
            if (andar > 0) {
                andarString = String.valueOf(andar);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        URIBuilder b = new URIBuilder("http://localhost:8080/catalogaBMA/faces/pegaLotePorQuery.jsp");
        if (!idLoteString.isEmpty()) {
            b.addParameter("id_Lote", idLoteString);
        }
        if (!andarString.isEmpty()) {
            b.addParameter("andar", andarString);
        }
        if (!(corredor == null) && !(corredor.equalsIgnoreCase(""))) {
            b.addParameter("corredor", corredor);
        }
        if (!(data_ini == null) && !(data_ini.equalsIgnoreCase(""))) {
            b.addParameter("data_ini", data_ini);
        }
        if (!(data_fim == null) && !(data_fim.equalsIgnoreCase(""))) {
            b.addParameter("data_fim", data_fim);
        }
        URL url = b.build().toURL();
        String urlString = url.toString();
        JSONObject jsob = respostaJson(urlString);
        List<CatalogaLote> listaLotes = new LinkedList<>();
        JSONArray jsArr = jsob.getJSONArray("data");
        for (int i = 0; i < jsArr.length(); i++) {
            JSONObject single = jsArr.getJSONObject(i);
            CatalogaLote lote = CatalogaBeanAndJson.jsonToCatalogaLote(single);
            listaLotes.add(lote);
        }
        return listaLotes;
    }

    public static List<FichaBean> getFichasLote(int idlote) throws IOException, JSONException, ParseException {
        String url = "http://localhost:8080/catalogaBMA/faces/pegaFichasPorLote.jsp?id_Lote=" + idlote;
        JSONObject jsob = respostaJson(url);
        List<FichaBean> listaFichas = FichaAndJson.jsonToListaFichas(jsob);
        return listaFichas;
    }

    public static boolean updateStatusFicha(int codficha, String status) throws IOException, JSONException, ParseException, URISyntaxException {
        URIBuilder b = new URIBuilder("http://localhost:8080/catalogaBMA/faces/updateStatusFicha.jsp");

        if (codficha > 0) {
            String codfichaString = String.valueOf(codficha);
            b.addParameter("codficha", codfichaString);
        }
        if (!(status == null) && !(status.equalsIgnoreCase(""))) {
            b.addParameter("status", status);
        }
        String input = b.toString();
        boolean sucesso = respostaUpdate(input);
        return sucesso;
    }

    public static void main(String[] args) throws IOException, SQLException, JSONException, ParseException, URISyntaxException {
//        TEste pega fichas
//        List<FichaBean> listaFichas = getFichasLote(2);

//Teste Update fichas
//        boolean sucesso = updateStatusFicha(2, "processado");
//        if(sucesso){
//            System.out.println("sucesso");
//        }
//Teste Pesquisa Lote
        List<CatalogaLote> listaLotes = pesquisaLote(0, 0, null, "2021-08-25", "2021-09-19");
    }
}

<%-- 
    Document   : cataa
    Created on : 15/09/2021, 16:19:14
    Author     : anibal
--%>



<%@page import="br.com.alexandria.catalogabma.rest.FichaAndJson"%>
<%@page import="java.util.List"%>
<%@page import="br.com.alexandria.catalogabma.rest.CatalogaBeanAndJson"%>
<%@page import="com.amazonaws.util.json.JSONObject"%>
<%@page import="java.util.Date"%>
<%@page import="br.com.alexandria.catalogabma.rest.QueryBeanAndJson"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSetMetaData"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="br.com.alexandria.catalogabma.DAO.*"%>
<%@page import="br.com.alexandria.catalogabma.beans.*"%>
<%@page import="br.com.alexandria.catalogabma.services.*"%>
<%@page import="java.io.IOException"%>
<%@page language="java" contentType="text/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.sql.Connection"%>
<%@page import ="java.sql.Statement"%>

<%
//http://localhost:8080/catalogaBMA/faces/cataa.jsp?id_Lote=1

    int idLote = 0;
    
    try {
        if (!(request.getParameter("id_Lote").isEmpty()) && !(request.getParameter("id_Lote") == null)) {
            String idLoteString = request.getParameter("id_Lote");
            idLote = Integer.parseInt(idLoteString);
        } else {
            idLote = 1;
        }
    } catch (Exception e) {
        e.printStackTrace();
    }
    
    CatalogaLote lote = CatalogaDAO.selectLotePorID(idLote);
    
    JSONObject fichasJson = FichaAndJson.pegaFichasPeloLote(lote);
    
    response.getWriter().write(fichasJson.toString());   

%>


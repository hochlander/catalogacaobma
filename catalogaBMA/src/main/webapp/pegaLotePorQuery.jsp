<%-- 
    Document   : cataa
    Created on : 15/09/2021, 16:19:14
    Author     : anibal
--%>



<%@page import="br.com.alexandria.catalogabma.rest.CatalogaBeanAndJson"%>
<%@page import="com.amazonaws.util.json.JSONObject"%>
<%@page import="java.util.Date"%>
<%@page import="br.com.alexandria.catalogabma.rest.QueryBeanAndJson"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSetMetaData"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="br.com.alexandria.catalogabma.DAO.*"%>
<%@page import="br.com.alexandria.catalogabma.beans.*"%>
<%@page import="br.com.alexandria.catalogabma.services.*"%>
<%@page import="java.io.IOException"%>
<%@page language="java" contentType="text/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.sql.Connection"%>
<%@page import ="java.sql.Statement"%>

<%
//http://localhost:8080/catalogaBMA/faces/pegaLotePorUrl.jsp?id_Lote=2
    
        String idLoteString = request.getParameter("id_Lote");
        String andarString = request.getParameter("andar");
        String corredor = request.getParameter("corredor");
        String data_iniString = request.getParameter("data_ini");
        String data_fimString = request.getParameter("data_fim");

    QueryBean bean = QueryBeanAndJson
            .stringToQueryBean(idLoteString, andarString, corredor, data_iniString, data_fimString);
    
    JSONObject jsonLotes = CatalogaBeanAndJson.geraJsonLotesPorIntervaloData(bean);   

    response.getWriter().write(jsonLotes.toString());   
    
  
%>


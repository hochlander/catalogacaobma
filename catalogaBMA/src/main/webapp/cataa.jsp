<%-- 
    Document   : cataa
    Created on : 15/09/2021, 16:19:14
    Author     : anibal
--%>



<%@page import="br.com.alexandria.catalogabma.rest.CatalogaBeanAndJson"%>
<%@page import="com.amazonaws.util.json.JSONObject"%>
<%@page import="java.util.Date"%>
<%@page import="br.com.alexandria.catalogabma.rest.QueryBeanAndJson"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSetMetaData"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="br.com.alexandria.catalogabma.DAO.*"%>
<%@page import="br.com.alexandria.catalogabma.beans.*"%>
<%@page import="br.com.alexandria.catalogabma.services.*"%>
<%@page import="java.io.IOException"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.sql.Connection"%>
<%@page import ="java.sql.Statement"%>

<%
//http://localhost:8080/catalogaBMA/faces/cataa.jsp?id_Lote=2&corredor=1B&andar=8&data_ini=2021-09-11&data_fim=2021-09-15
    
        String idLoteString = request.getParameter("id_Lote");
        String andarString = request.getParameter("andar");
        String corredor = request.getParameter("corredor");
        String data_iniString = request.getParameter("data_ini");
        String data_fimString = request.getParameter("data_fim");

    QueryBean bean = QueryBeanAndJson
            .stringToQueryBean(idLoteString, andarString, corredor, data_iniString, data_fimString);
    
    JSONObject retorno = CatalogaBeanAndJson.pegaLotePorJson(bean);   
    
  
%>

<div >
  <%=retorno%>
</div>
